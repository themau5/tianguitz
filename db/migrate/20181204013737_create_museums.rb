class CreateMuseums < ActiveRecord::Migration[5.2]
  def change
    create_table :museums do |t|
      t.string :photo
      t.text :description
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :museums, [:user_id, :created_at]
  end
end
