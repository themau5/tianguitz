class CreateStores < ActiveRecord::Migration[5.2]
  def change
    create_table :stores do |t|
      t.string :store_name
      t.text :store_description
      t.decimal :price
      t.string :currency
      t.text :description
      t.string :image
      t.integer :discount
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
