class MuseumsController < ApplicationController
  before_action :logged_in_user, only: [:index, :create, :destroy]
  before_action :correct_user,   only: :destroy
  
  def index
    @museum = Museum.all
    @museum  = current_user.museums.build
    @feed_mitems = current_user.feedm.paginate(page: params[:page])
  end
    
  def create
    @museum = current_user.museums.build(museum_params)
    if @museum.save
      flash[:success] = "Publicación Exitosa!"
      redirect_to museums_path
    else
      @feed_mitems = []
      render 'museums/index'
    end
  end

  def destroy
    @museum.destroy
    flash[:success] = "Publicación borrada"
    redirect_to  museums_path
  end

  private
  
  def museum_params
      params.require(:museum).permit(:description, :photo)
  end
  
  def correct_user
    @museum = current_user.museums.find_by(id: params[:id])
    redirect_to root_url if @museum.nil?
  end
end
