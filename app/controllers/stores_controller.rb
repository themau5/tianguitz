class StoresController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy
  
  def show
    @store = Store.find(params[:id])
  end
  
  def new
    @store = Store.new
  end
  
  def create  
    @store = current_user.stores.build(store_params)
    if @store.save
      flash[:success] = "Felicidades, has creado tu primer puesto!"
      redirect_to request.referrer
    else 
      @feed_items = []
      render 'static_pages/home'
    end
  end
  
  
  private
  
    def store_params
      params.require(:store).permit(:store_name, :store_description, :price, :currency, 
      :description, :image, :discount)
    end
    
    def correct_user
      @store = current_user.stores.find_by(id: params[:id])
      redirect_to root_url if @store.nil?
    end
  
end
