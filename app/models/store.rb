class Store < ApplicationRecord
  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :store_name, presence: true
  validates :description, presence: true
end
