class Museum < ApplicationRecord
  belongs_to :user
  validates :user_id, presence: true
  mount_uploader :photo, PostpictureUploader
  default_scope -> { order(created_at: :desc) }
  validates :description, presence: true, length: { maximum: 140 }
  
  private
  
  # Validates the size of an uploaded picture.
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:photo, "Imágen debe ser menos de 5mb")
      end
    end
end
