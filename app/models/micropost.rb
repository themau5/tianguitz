class Micropost < ApplicationRecord
  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  mount_uploader :picture, PostpictureUploader
  validates :content, presence: true, length: { maximum: 140 }
  validate :picture_size
  
  private
  
  # Validates the size of an uploaded picture.
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "Imágen debe ser menos de 5mb")
      end
    end
  
end
